# `Simple Status`

## Introduction
  This repo is a very simple `Status bar` for `Window Manager`.

  Has feature of **real demand**.(At least to me).

  Add new feature when **found new real demand**.

## Feature
  + *DateTime*
  
  + *Battery*
  
  `Contiune`

## Usage
  + `Install`
  
    `make`
    
    `sudo make install`
    
    Default, the program **install to** `/usr/lib/bin`.

    Need use **root** account to install.

  + `Clean`
  
    ```
    make clean
    ```

## Screenshot

   ![Desktop](https://bitbucket.org/kypi/simplestatus/raw/9a0ec591e778bda8cb72dcea60cfbdadc0add5ba/screenshot.png)